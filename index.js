const express = require('express');
const geoip = require('geoip-lite');

const app = express();

const port = process.env.PORT || 3001;

app.use(express.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json'
  );
  next();
});

app.get('/country', (req, res) => {
  const ip = req.headers['x-forwarded-for'];
  const country = geoip.lookup(ip).country;
  res.send(country);
});

//Prueba Mexico
// app.get('/MX', (req, res) => {          
//    const ip = '131.178.0.0';
//    const country = geoip.lookup(ip).country;
//    res.send(country);
//  });
  
//Prueba EU
// app.get('/US', (req, res) => {
//    const ip = '3.0.0.0';
//    const country = geoip.lookup(ip).country;
//    res.send(country);
//  });

app.listen(port);
